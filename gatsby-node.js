const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
              }
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog posts pages.
  const posts = result.data.allMarkdownRemark.edges

  const getLang = post =>
    post.node.fields.slug.startsWith("/fr") ? "fr" : "en"

  posts.forEach((post, index) => {
    const lang = getLang(post)
    const sameLangPosts = posts.filter(otherPost => getLang(otherPost) === lang)
    const sameLangIndex = sameLangPosts.indexOf(post)
    const previous =
      sameLangIndex === sameLangPosts.length - 1
        ? null
        : sameLangPosts[sameLangIndex + 1].node
    const next =
      sameLangIndex === 0 ? null : sameLangPosts[sameLangIndex - 1].node

    createPage({
      path: post.node.fields.slug,
      component: blogPost,
      context: {
        lang,
        slug: post.node.fields.slug,
        previous,
        next,
      },
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
