import React from "react"
import {localizedPath} from "../i18n-config"
import Link from "gatsby-link"
import withLang from "./withLang"

export default withLang(props => {
  const { lang, to, children } = props
  return <Link exact to={localizedPath(to, lang)} children={children} />
})
