import React from "react"
import { graphql, useStaticQuery } from "gatsby"

import { rhythm } from "../utils/typography"
import Image from "gatsby-image"
import withLang from "./withLang"

const Bio = ({ lang }) => {
  // const data = useStaticQuery(graphql`
  //   query BioQuery {
  //     avatar: file(relativePath: { eq: "profile-pic.jpg" }) {
  //       childImageSharp {
  //         fixed(width: 50, height: 50) {
  //           ...GatsbyImageSharpFixed
  //         }
  //       }
  //     }
  //   }
  // `)

  return (
    <div
      style={{
        display: "flex",
        marginBottom: rhythm(2.5),
      }}
    >
      {/*<Image*/}
      {/*    fixed={data.avatar.childImageSharp.fixed}*/}
      {/*    alt="Samuel Bouchet"*/}
      {/*    style={{*/}
      {/*        marginRight: rhythm(1 / 2),*/}
      {/*        marginBottom: 0,*/}
      {/*        minWidth: 50,*/}
      {/*        borderRadius: `100%`,*/}
      {/*    }}*/}
      {/*    imgStyle={{*/}
      {/*        borderRadius: `50%`,*/}
      {/*    }}*/}
      {/*/>*/}
      <div>
        {lang === "fr" ? (
          <span>
            Écrit par <strong>Samuel Bouchet</strong> à Nantes.
          </span>
        ) : (
          <span>
            Written by <strong>Samuel Bouchet</strong> who lives and works in
            Nantes.
          </span>
        )}
        <div>
          <a href="https://twitter.com/lythom">
            <span>@Lythom</span>
          </a>
        </div>
      </div>
    </div>
  )
}

export default Bio
