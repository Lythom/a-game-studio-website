---
title: A Time Paradox
date: "2018-08-27"
---

Your are stuck in a timeloop. Infiltrate against yourself to escape paradoxes !

<div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="320" height="320">
        <source src="atimeparadox_loop.mp4" type="video/mp4" />
    </video>
</div>

> I must hide from my past selves. If one see me…  
> <cite>A time traveller</cite>

<div class="startButton">
    <a href="https://a-time-paradox.com/">Visit website</a>
</div>
