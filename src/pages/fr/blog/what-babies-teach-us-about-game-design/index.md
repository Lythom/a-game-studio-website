---
title: Ce que les bébés nous apprennent sur le game design
date: "2018-09-14"
description: Découvrez en quoi les études sur le développement du cerveau et sur nos émotions nous aide en tant que game designer.
---

> « Entre vingt et cinquante fois par jour, le petit enfant vit un état de grand enthousiasme. (...) Chaque petite tempête d'enthousiasme met en œuvre une sorte d’autodoping cérébral. Ainsi sont produites les substances nécessaires à tous les processus de croissance et de réaménagement des réseaux neuronaux. C’est ce qui explique pourquoi nous progressons si rapidement dans ce que nous faisons avec enthousiasme. Car c’est aussi simple que cela : le cerveau se développe précisément là où il est utilisé avec enthousiasme. » <cite>Pr. Gerald Hüther</cite>

<figure>
  <img src="lego-3388163_640.png" alt="Dessin d'une jeu de construction pour enfants">
  <figcaption>Illustration par <a href="https://pixabay.com/fr/users/Painter06-3732158/">Francis Ray</a></figcaption>
</figure>

## Sur le développement du cerveau

En plus de nous exposer des mécaniques liées à la croissance, précieuses à connaître pour guider son enfant vers un meilleur épanouissement, la conférence « Neurobiologie et éducation » du Professeur Gerald Hüther éclaire plus généralement sur le fonctionnement du cerveau.

Des études montrent[^1]<sup>,</sup>[^2] qu'une poussée d'enthousiasme provoque un déversement de transmetteurs neuroplastiques. De la même façon qu'un engrais, leur rôle est de favoriser le développement du cerveau. Trois choses importantes sont à noter :

- seules les zones _actives_ du cerveau sont concernées par ce développement,
- cela ne survient que lorsqu'on ressent une émotion puissante,
- cette mécanique fonctionne quel que soit l'âge.

Anecdote de l'auteur, pertinente dans notre contexte :

> « Nos jeunes ont, depuis 10 ans, une région du cerveau qui reçoit tant d'engrais qu'elle a déjà doublé de taille : il s'agit de la région qui est chargée de la régulation des mouvements du pouce. <cite>Pr. Gerald Hüther</cite>

Ainsi, on retiendra que pour transmettre un message, encourager certains comportements, favoriser la mémorisation d'éléments importants, il faut que le joueur soit touché émotionnellement.

## Sur l'exclusion sociale

On apprend aussi dans cette conférence que lorsqu'on subit une isolation ou une exclusion sociale cela active les mêmes circuits neuronaux que lorsqu'on nous inflige des souffrances corporelles .

Sous cette lumière, la dimension inclusive d'un jeu devient primordiale : il n'est pas envisageable de produire un jeu qui crée de la souffrance.

Tiré de mon expérience, un exemple concret est celui du jeu de société [Dixit](https://fr.asmodee.com/fr/games/dixit/). Dans ce jeu le but est de faire deviner une carte à certains joueurs, mais pas à tous. En conséquence les joueurs ayant déjà des liens sociaux forts ont tendance à resserrer leurs liens encore plus, mais les joueurs plus éloignés du groupe social majoritaire sont en revanche naturellement exclus.

<figure>
  <img src="mysterium.jpg" alt="Cartes du Mysterium">
  <figcaption style="text-align:center">Exemple de cartes "vision" de Mysterium</figcaption>
</figure>

Fort de ce constat, le tir est corrigé dans [Mysterium](https://fr.asmodee.com/fr/games/mysterium/), identique dans ses mécaniques (faire deviner des cartes), mais dont l'approche collaborative incite naturellement les joueurs à mieux se connaître pour réussir. Les concepteurs ont réussi à conserver une mécanique de jeu vraiment amusante (Dixit reste un très bon jeu !) tout en renversant les relations entre les joueurs : les nouveaux objectifs de jeu promeuvent l'empathie encore davantage (on essaye d'imaginer comment l'autre joueur va percevoir un visuel) et la mécanique discriminante disparait (même si une sous-partie du groupe seulement devine, tout le groupe bénéficie de la découverte).

## Pour nous et pour tous

Dans les relations avec nos enfants comme avec nos joueurs, nous gagnons à mieux connaître le fonctionnement du cerveau pour proposer des expériences plus épanouissantes. En tant que concepteurs (mais pas seulement), les neurosciences ont encore probablement beaucoup à nous apprendre. Sachons nous adapter à ces nouvelles connaissances !

---

Envie de réagir ? Mentionnez [@Lythom](https://twitter.com/lythom) dans votre tweet ou envoyer un mail à [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).

[^1]: [Natures and value - Pr. Dr. Gerald Hüther](https://www.ecologiedelenfance.com/app/download/14155822524/nature_and_values.pdf?t=1492608224)
[^2]: [Neurobiologie et éducation : conférence du Prof. Dr. Gerald Hüther (VOSTFR)](https://youtu.be/IGQ9i-xdruc)
