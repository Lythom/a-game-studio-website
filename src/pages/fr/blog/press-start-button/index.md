---
title: Appuyez sur Start
date: "2018-08-26"
description: Découvrez le blog de A Game Studio !
---

A Game Studio ouvre son blog ! Vous trouverez ici des actualités, des articles techniques sur la programmation, les shaders, le game design et sur tout sujet intéressant qu'un développeur de jeux indépendant peut aborder.

J'ai hate ! J'espère que ça va vous plaire !

Sans plus tarder, je vous emmène vers le premier article technique, que le fun commence !

<div class="startButton">
    <a href="../hacking-through-shaders">Start</a>
</div>
