---
title: Mise à jour prochaine de A Time Paradox
date: "2019-11-11"
description: Découvrez les nouveautés à venir dans A Time Paradox
---

A Time Paradox est toujours en développement ! Pour ceux qui ne connaissent pas c'est un jeu de réflexion que je suis en train de développer dont la mécanique principale est l'infiltration contre soi-même.
Comme le projet est auto-financé, il progresse de façon irrégulière en fonction des prestations et événements de la vie. Où en est-il ? Et bien puisque vous demandez… de plus en plus proche de son but !

## Dans le jeu

La version Android est toujours en accès anticipé, mais touche à sa fin. Le mode défi est techniquement terminé et le mode histoire a été retravaillé pour être plus abordable. Là où le mode histoire vous permettra de progresser petit à petit, le mode défi est fait pour explorer les limites de votre esprit ! N'hésitez pas à reprendre l'histoire si vous aviez du vous arrêter à cause d'un niveau infranchissable, les barrières de l'extrême sont maintenant dans les défis.

<figure>
  <img src="defi_example.jpg" alt="">
  <figcaption style="text-align:center">Exemple de défi à venir</figcaption>
</figure>

Vous pourrez aussi créer vos propres expériences grâce à l'éditeur de niveau intégré ! Simple outil d'amusement dans un premier temps, il pourra peut être vous permettre plus tard de créer des expériences plus grandes pour les autres joueurs.

<figure>
    <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="480" height="auto">
        <source src="editor_showcase_480.mp4" type="video/mp4" />
    </video>
    </div>
  <figcaption style="text-align:center">Démonstration de l'éditeur de niveau</figcaption>
</figure>

Enfin, c'était un gros morceau à développer telle la partie cachée d'un iceberg, il vous sera proposé si vous le souhaitez de créer un compte "A Game Studio". Sans mot de passe, avec juste votre adresse mail, vous aurez le choix de synchroniser votre progression en ligne. Ainsi, vous pourrez récupérer vos clés, votre progression et vos records si vous changez de téléphone ou pour jouer sur plusieurs périphériques à la fois. A noter que vos données personnelles (e-mail et données de jeux) ne seront pas transmises à des tiers, et que les serveurs A Game Studio sont localisés en Europe.

<figure>
    <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="360" height="auto">
        <source src="unlock_levels.mp4" type="video/mp4" />
    </video>
    </div>
  <figcaption style="text-align:center">Débloquez tous les défis !</figcaption>
</figure>

Sauvegarder ses clés ? Et oui ! Les défis se débloquent en collectionnant des clés. Vous pourrez en récupérer de multiple façon, dans certains niveaux du jeu lui-même ou chaque semaine en vous connectant. A noter que les clés ne seront jamais payantes directement. La promesse reste inchangée : un achat unique pour profiter de l'intégralité de l'expérience du jeu. Il vous faudra mériter certains contenus cependant !

## Dans l'internet

Jouer seul c'est sympa, et partager est une expérience plus grande encore ! Aussi, A Time Paradox est présent sur [Discord](https://discord.gg/UVZ27gA)), sur [Twitter](https://twitter.com/AGameStudio)), et sur [Facebook](https://www.facebook.com/atimeparadox/). Si vous voulez juste les actualités sans fioriture sociale, le [flux RSS de ce blog](https://a-game-studio.com/rss.xml) ou [la newsletter](https://a-time-paradox.com/#mc_embed_signup_scroll) seront vos meilleurs choix.

## Dans votre PC

Cette mise à jour est aussi une première étape préparatoire à la sortie PC du jeu ! Si la mise à jour Android viendra dans les prochaines semaines, la version PC prendre encore quelques semaines de plus. Elle contiendra un mode exclusif qui n'aurait pas été possible sur mobile…

Si vous êtes plutôt joueur PC, je vous invite à ajouter [A Time Paradox](https://store.steampowered.com/app/1167730/A_Time_Paradox/) à votre liste de souhaits sans plus tarder.

<figure>
  <img src="in_library.png" alt="Jeu A Time Paradox dans la bibliothèque de jeux steam">
  <figcaption style="text-align:center">Bientôt dans votre bibliothèque</figcaption>
</figure>

J'espère que A Time Paradox vous intéresse autant qu'il me passionne ! A bientôt pour de nouvelles aventures temporelles !

---

Envie de réagir ? Mentionnez [@Lythom](https://twitter.com/lythom) dans votre tweet ou envoyer un mail à [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).
