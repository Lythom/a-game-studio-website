---
title: 2020, une année de paradoxes temporels ?
date: "2020-01-03"
description: Mise à jour de A Time Paradox, Bonne année !
---

Bonne Année 2020 !

Si entrainer votre cerveau fait partie de vos bonnes résolutions pour 2020, vous devriez aimer cette mise à jour de A Time Paradox ! Voici un aperçu de ce qu'elle contient.

Elle est déjà disponible sur Android :
[A Time Paradox sur Play Store](https://play.google.com/store/apps/details?id=studio.agame.atimeparadox)

## Version anglaise

A Time Paradox est maintenant disponible dans la plupart des pays du monde, et vous pourrez le partager avec vos amis à l'étranger pour peu qu'ils parlent français ou anglais.

<figure>
        <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="720" height="auto" controls>
        <source src="menurotate_720.mp4" type="video/mp4" />
    </video>
</div>
  <figcaption style="text-align:center">Menu rotatif de A Time Paradox</figcaption>
</figure>

## Jouez avec un ami !

L'aperçu de la nouvelle campagne coopérative est disponibles. Dix niveaux sont déjà jouables à 2 sur le même apppareil et d'autres sont prévus !

## Rééquilibrage du mode histoire

Vous étiez coincé ? Les niveaux trop durs ont été retravaillé ! Il y a même des clés de défi à collectionner dans certains niveaux.

## Défis

Ces clés permettent de débloquer des niveaux de défi. Si vous avez déjà terminé le jeu, voilà de quoi s'amuser encore !

Des lots de clés sont offerts en se connectant au jeu chaque semaine (nécessite une connexion internet).

## Éditeur de niveaux

L'éditeur de niveau enrichit encore plus l'expérience de A Time Paradox en vous permettant de créer vos propres défis. Venez les partager sur le [Discord](https://discord.gg/UVZ27gA) !

<figure>
        <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="480" height="auto" controls>
        <source src="editor_showcase_480.mp4" type="video/mp4" />
    </video>
</div>
  <figcaption style="text-align:center">Démonstration de l'éditeur de niveaux</figcaption>
</figure>

## Sauvegarde de la progression en ligne

Après avoir acheté le jeu, il vous sera proposé de créer un compte "A Game Studio" pour synchroniser votre progression en ligne. Ne perdez plus vos clés de défis ni votre avancement en changement de matériel !
Le jeu restera toujours jouable sans internet.

## Option de son

Vous pourrez désactiver/activer les sons désormais, je parie que certain d'entre vous apprécieront de jouer avec leurs musiques préférées.

# Amusez-vous bien !

J'attends vos retours avec impatience sur toutes ces nouveautés, amusez-vous bien !

Samuel

[https://a-time-paradox.com/](https://a-time-paradox.com/)

---

Envie de réagir ? Mentionnez [@Lythom](https://twitter.com/lythom) dans votre tweet ou envoyer un mail à [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).
