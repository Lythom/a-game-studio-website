---
title: Unity étape par étape - jeu de ballon
date: "2020-05-21"
description: Cet article tutorial explique étape par étape comment réaliser un jeu de ballon avec Unity.
---

Aperçu: <br>
→ [Installer Unity](#installer-unity) <br>
→ [Afficher un sprite](#afficher-un-sprite) <br>
→ [Animer un sprite](#animer-un-sprite) <br>
→ [Les règles du jeu](#les-règles-du-jeu) <br>
→ [Le moteur physique](#le-moteur-physique) <br>
→ [Trucs et astuces](#trucs-et-astuces) <br>

[Note: Article en cours d'écriture, il n'est pas encore finalisé. Merci de votre compréhension]

Cet article a vocation de tutorial et de support de cours pour apprendre les bases de Unity en 2D.
Aucun pré-requis n'est nécessaire, mais il se peut que vous deviez explorer certaines fonctionnalités de Unity par vous même pour réussir certaines étapes.

Il s'agira de développer avec Unity un jeu de ballon, prétexte pour apprendre des notions clés tel que :

- L'initialisation d'un projet
- La configuration d'un environnement de développement
- l'importation de sprites
- les scripts pour manipuler les entités (camera, personnage)
- le rendu de sprites animés (via script et via Animator)
- le moteur physique 2D
- les règles du jeu via scripting

## Installer Unity

1. Télécharger et installer [Unity Hub](https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe)
2. Depuis le hub, installer la dernière version stable de Unity (Installs > Add > Latest Official Release)
3. Dans les modules,

- installer Visual Studio Community 2019 si vous souhaitez l'utiliser comme environnement de développement (IDE). Nous utiliserons plutôt Visual Studio Code (VSCode) pour la suite de ce tutorial.
- installer les platformes pour lesquelles vous souhaiter compiler votre projet, au moins une, par exemple "Windows build support" si vous êtes sous windows.

4. Télécharger et installer [Visual Studio Code](https://code.visualstudio.com/)
5. Dans VSCode, installer les extensions "C#" et "Debugger for Unity"
6. Créer un nouveau projet depuis Unity HUB (Projects > New > 2D), choisir un nom et un dossier de destination
7. Dans Unity, configurer votre éditeur de script externe pour être VSCode en installant le paquet "Visual Code Extension depuis le package manager".
   ![](package_manager.png)
8. Suivre la manipulation suivante pour configurer l'éditeur externe dans "edit > Preferences > External Tools".

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="configure_vcode.mp4" type="video/mp4" />
</video>
</div>

## L'interface

![](unity_interface.png)

Les onglets manipulés fréquement sont :

- **La hiérarchie** : va de paire avec la scène. Dans Unity on manipule des objets `GameObject` qu'on peut hierarchiser dans la scène. un objet peut avoir des enfants et être situé dans un parent. Lorsqu'un parent est déplacé, redimensionné ou tourné, alors les objets enfants subissent la transformation relative à leur parent.
- **La scène** : Les objets ayant des composants visuels sont affichés sur la scène. Ils peuvent être manipulés (déplacements, etc.) via les outils de la barre d'outils. Les objets n'ayant pas de composants visuels sont également manipulés mais ne peuvent être sélectionnés que via l'onglet hiérarchie.
  Il est fréquent d'avoir des scènes différentes pour différentes étapes du jeu et de basculer d'une scène à l'autre via des scripts.
- **Le jeu** : Lorsqu'on clique sur le triangle "lecture" en haut de la scène, Unity démarre le jeu et bascule directement sur l'onglet "jet" qui affiche le rendu final du jeu. Ce rendu est celui de la (ou des) caméra(s) configurée(s) dans la scène.
- **L'inspecteur** : c'est là qu'on peut modifier les propriétés de l'objet sélectionné (qui peut être sur la scène/hierarchie ou dans la vue projet) et de ces composants. C'est également là qu'on peut ajouter à l'objet des nouveaux composants. Un composant est un script qui hérite de `MonoBehaviour` et qui enrichit l'objet de nouveaux comportements. Certains sont fournis par défaut avec Unity (par exemple `SpriteRenderer` pour afficher une image) et il est souvent nécessaire de créer des composants personnalités (via le bouton "Add Component > New Script" de l'inspecteur).
- **Le projet** : Rattaché au système de fichiers du système d'exploitation, c'est à dire au dossier du projet, c'est là qu'on ajoutera toutes les assets (images, sons, scripts, etc.) qui serviront au fonctionnement du projet.
- **La console** : C'est là qu'apparaitront les erreurs de compilations et les Exceptions non capturées lors de l'exécution (runtime). A consulter lorsque tout ne se passe pas comme prévu.

## Afficher un sprite

1. Ajouter une image (glisser déposer) depuis votre exlorateur de fichier vers la vue projet OU créer le fichier image dans le dossier `Assets/` de votre projet.
2. Dans la vue projet, sélectionner l'image puis regarder l'inspecteur: il affiche les informations d'importation de texture.
   ![](sprite_importer.png)

- En projet 2D la texture est automatiquement configurée de type "sprite (2D and UI)". Pour texturer un objet 3D on utiliserait "default". Les autres modes sont pour des usages avancés (vous le saurez quand vous en aurez besoin).
- "PixelsPerUnit" Indique à Unity quelle sera la densité de pixels à utiliser par rapport au système d'unité du moteur. Unity est un moteur 3D, et comme la notion de "pixel" n'existe pas dans un espace 3D Unity propose un système d'unité de référence. Ces unités sont totalement arbitraire (on dira "une taille de une unité" ou "une taille de un") mais permettent de garantir la cohérence des proportions entre les assets quelle que soit la position de la caméra et la résolution finale. Nous n'apprendrons pas à afficher un sprite en rendu pixel perfect (1px du sprite rendu sur 1px dans le rendu final), si vous souhaitez le faire voici un tutorial : [https://blogs.unity3d.com/2019/03/13/2d-pixel-perfect-how-to-set-up-your-unity-project-for-retro-8-bits-games/](https://blogs.unity3d.com/2019/03/13/2d-pixel-perfect-how-to-set-up-your-unity-project-for-retro-8-bits-games/)
- Si vous utilisez du pixel art, passer le "filter mode" en "Point (no filter)" pour un meilleur rendu.

3. Depuis la vue projet, glisser déposer l'image vers la scène. Celà créera automatiquement un `GameObject` avec un composant `SpriteRenderer` configurer pour afficher votre image.

### Autres exemples courants

#### Sprite de fond avec répétition

![](sprite_tiledbackground.png)

Particularités :

- Draw Mode: Tiled (préciser le nombre de répétitions dans Width et Height)
- (Configuration d'importation de l'image) Mesh Type: FullRect

#### Forme géométrique de base (ex. hexagone)

![](sprite_hexa.png)

Particularité :

- Créé depuis la vue Project via "clic droit > Create > Sprite > Hexagon"

## Animer un sprite

Deux méthodes seront présentées ci-dessous pour animer des sprites: avec un script ou avec l'outil Animator de Unity.

Pour toutes les méthodes : créer un sprite multiple à partir d'une image spritesheet, extraire les séquences en sélectionnant les images voulues dans l'onglet projet et en les déplaçant vers la scène.

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="Unity_create_animated_b2viup.mp4" type="video/mp4" />
</video>
</div>

Image de référence pour le sprite :

![](girl.png)

### Option 1: Animer à la Unity

- Configurer le Controller au niveau de l'Animator qui a été ajouté automatiquement sur un des game object créés, ajouter des transitions basées sur des propriétés pour basculer d'une animation à l'autre.

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="Unity_controller.mp4" type="video/mp4" />
</video>
</div>

- Utiliser un script pour modifier la propriété du controller.

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="Unity_animate_script_zwunac.mp4" type="video/mp4" />
</video>
</div>

#### AnimateGirl.cs

```CS
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimateGirl : MonoBehaviour {

    Animator animator;

    private void Start() {
        animator = GetComponent<Animator>();
    }

    void Update() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            animator.SetFloat("Speed", 2f);
        } else {
            animator.SetFloat("Speed", 0f);
        }
    }
}

```

### Option 2: Animer un sprite via script

Personnellement, j'évite le plus possible d'utiliser les Controller et Animator de Unity pour la 2D: ils apportent énormément de complexité pour simplement faire défiler des sprites. Aussi je préfère écrire moi-même un script qui va venir modifier les images du SpriteRenderer à fréquence régulière pour créer l'animation.

Je crée un script "SpritesheetAnimator" qui s'occupe d'accepter une configuration de séquences pour un gameObject et de modifier l'image du SpriteRenderer Associé, et un deuxième script comme précédemment pour piloter l'animation en fonction du gameplay.

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="Unity_animate_spritesheetscript_qexv4u.mp4" type="video/mp4" />
</video>
</div>

#### SpritesheetAnimator.cs

```CS
using System;
using System.Linq;
using UnityEngine;

// Lister toutes les animations possibles ici
public enum Anims {
    Iddle,
    Run,
    Roll
}

// Structure de données pour définir une animation
[Serializable]
public struct AnimDefinition {
    public Anims name;
    public Sprite[] frames;
}

// L'animator va changer le sprite du SpriteRenderer à une certaine fréquence (par défaut 25 fois par secondes)
[RequireComponent(typeof(SpriteRenderer))]
public class SpritesheetAnimator : MonoBehaviour {

    [Tooltip("animation speed in images per seconds")]
    public float animationSpeed = 25;

    public AnimDefinition[] animations;

    // accessors (getters)
    public AnimDefinition CurrentAnimation => currentAnimation;
    public int LoopCount => loopCount;
    public float animationFrameDuration => 1f / animationSpeed;

    // private properties
    private SpriteRenderer spriteRenderer;
    private AnimDefinition currentAnimation;

    private int currentFrameIndex = 0;

    // durée en seconde avant l'affichage de la prochaine image
    private float nextFrameCoolDown;

    // nombre de fois que l'animation est jouée complètement
    private int loopCount = 0;

    void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        nextFrameCoolDown = animationFrameDuration;
        currentAnimation = animations[0];
    }

    void Update() {
        if (currentAnimation.frames.Length == 0) return;

        // déclenche la frame suivante si le temps est écoulé
        if (nextFrameCoolDown <= 0) {
            AnimateNextFrame();
        }

        // si on change la vitesse de l'animation, raccourci le temps d'attente si nécessaire
        if (animationFrameDuration < nextFrameCoolDown) nextFrameCoolDown = animationFrameDuration;

        // écoule le temps avant la prochaine image
        nextFrameCoolDown -= Time.deltaTime;
    }

    public void AnimateNextFrame() {
        // calcule l'indice de l'image suivante
        currentFrameIndex = (currentFrameIndex + 1) % currentAnimation.frames.Length;
        // affiche l'image suivante
        spriteRenderer.sprite = currentAnimation.frames[currentFrameIndex];
        // Ajoute un temps d'attente calculé à partir de la fréquence de l'animation
        nextFrameCoolDown += animationFrameDuration;
        // compte le nombre de fois que l'animation a été jouée
        if (currentFrameIndex == 0) loopCount++;
    }

    public void Play(Anims nextAnimation) {
        // Si l'animation est déjà jouée, on ne fait rien
        if (currentAnimation.name == nextAnimation) return;
        // trouve l'animation correspondante dans la liste
        currentAnimation = animations.First(a => a.name == nextAnimation);
        // réinitialise à la première image de la séquence
        currentFrameIndex = 0;
        loopCount = 0;
    }
}
```

#### AnimateGirlSpritesheet.cs

```CS

using UnityEngine;

[RequireComponent(typeof(SpritesheetAnimator))]
public class AnimateGirlSpritesheet : MonoBehaviour {
    SpritesheetAnimator animator;

    void Start() {
        animator = GetComponent<SpritesheetAnimator>();
    }

    void Update() {
        if (Input.GetKey(KeyCode.RightArrow)) {
            animator.Play(Anims.Run);
        } else if (Input.GetKey(KeyCode.DownArrow)) {
            animator.Play(Anims.Roll);
        } else {
            animator.Play(Anims.Iddle);
        }
    }
}
```

## Le debugger

Debugger (déboguer en français, peu utilisé), permet d'inspecter le code alors qu'il est en train d'être exécuté par le programmes. Avec Unity, votre outillage VSCode (avec les plugins installés précédemment) vous permettent d'utiliser directement cette fonctionnalité.

### 1er cas: le fichier launch.json n'existe ppas dans le projet

Pour le configurer (première utilisation uniquement), accéde à l'onglet debug (1) puis créez un fichier launch.json si proosé (2).

![1. ouvrir sur l'onglet debugger, 2. ouvrir sur "create  launch.json file](vscode_enable_debug.png)

### 2eme cas: le fichier existe déjà et "Unity debugger" ne fait pas partie des environnements proposés

Dans ce cas ajouter l'environnement comme indiqué dans l'image ci-dessous.

![1. ouvrir sur l'onglet debugger, 2. ouvrir la liste des environnements, 3. choisir "Add configuration"](vscode_debug_add_env.png)


Choisissez l'environnement "Unity debugger".

![sélectionner "Unity debugger" dans la liste des envrionnement](vscode_debug_env.png)


### Utiliser le debugger

Une fois l'environnement d'exécution "Unity Debugger" sélectionner, lancer "l'attache" en cliquant sur le triangle vert à côté de l'environnement.

L'outil de debugging de VSCode va alors s'attacher au debugger intégré à Unity, et vous pourrez alors commencer à mettre des points d'arrêts (_breakpoint_ en anglais).

Pour mettre un point d'arrêt cliquer sur la marge gauche d'un ligne de code que vous souhaitez inspecter.

![](vscode_debug_ex_breakpoint.png)

Démarrer le jeu et faites en sorte que la ligne qui contient le point d'arrêt soit atteinte. Le jeu va alors se mettre en pause et la fenêtre se transformer de la façon suivante :

![](vscode_debug_stopped2.png)

- (1) La ligne où le debugger est arrêté est surlignée en jaune
- (2) Une barre de navigation en haut à droite permet de controler l'exécution du programme: reprendre, avancer de une ligne, debugger l'intérieur de la fonction, terminer la fonction et debugger à la sortie, relancer le debugger (interrompt la session en cours), arrête de debugger.
- (3) en haut à gauche l'encart "variables" permet d'inspecter les variables du contexte en cours
- (4) à gauche milieu "watch" permet de saisir des expressions à évaluer et d'afficher le résultat par rapport au contexte en cours
- (5) à gauche en bas "call stack" permet de remonter dans la pile d'appel du programme (trouver les contextes des fonctions appelantes)
- (6) à gauche en bas "breakpoints" permet de gérer la liste des breakpoints actuellement acquis
- (7) il est possible de survoler les variables avec le souris pour connaitre leur valeur (ex: maxDistancePerFrame dans cette image)


## Le moteur physique

Pour le vocabulaire et les notions clés :

- https://teachings.samuel-bouchet.fr/jeu-video/essentiel_physique#/

### Déplacement d'un personnage dans un univers physique

1. Ajouter un Rigidbody2D configuré en dynamique. Cocher la case "Contraints: Freeze rotation Z" pour que le sprite ne tourne pas sur lui même.

2. Ajouter un CircleCollider2D. Il est possible d'ajouter plusieurs collider2D sur un même gameObject pour créer une forme composée, il en faut au moins un. Ici le circle collider eut être édité en cliquant sur le bouton "edit collider" et en modifiant la taille et la position du cercle qui apparait sur le personnage.

![](physics.png)

Ensuite, il faudra utiliser un script pour déplacer le personnage. Lorsqu'on travaille avec le moteur physique de Unity :

- on doit toujours déplacer l'objet en modifiant le RigidBody2D. On par exemple le déplacer en utilisant dans un script `rigidbody2D.velocity = new Vector2(1, 0)` pour donner une vitesse de 1 unité par seconde vers la droite, le moteur physique se chargera de mettre à jour la position.
- il est préférable de faire cette action dans une fonction `void FixedUpdate() {` plutôt que dans l'habituel `void Update() {` car FixedUpdate est appelé à chaque étape du moteur physique alors que Update est appelé à chaque rendu de frame du moteur, les 2 n'étant pas forcément synchronisés en fonction de la configuration du moteur de jeu et des performances de la machine.


## Les règles du jeu

- Créer des buts sur les côtés
- Lorsque la balle entre dans un but, 
    - +1 point pour l'équipe de la couleur opposée
    - la balle est remise au milieu
    - les joueurs sont repositionnés de chaque côté
- lorsqu'une équipe à 3 points
    - l'équipe est indiquée vainqueur
    - les points sont remis à 0
- les joueurs sont bloqués par la balle et ne la font pas bouger
- les joueurs sont attaché à un marteau par un lien de longueur fixe, en bougeant ils peuvent donner de l'inertie et faire tourner le marteau


## Trucs et astuces

Source : https://teachings.samuel-bouchet.fr/jeu-video/4_fichespratiques#/4/1

### Prefabs

Les prefabs permettent de créer un gameObject "modèle" qui pourra ensuite être instancié sur la scène autant de fois qu'on le souhaite à l'identique. Il est aussi possible d'instancier des prefabs dynamiquement via des scripts.

- Glisser/Déposer un objet d'une scène dans le projet pour faire un prefab
- Glisser/Déposer un prefab sur la scène pour créer une instance liée
- Mettre à jour prefab = update sur toutes les instances liés
- Mettre à jour instance liée = possibilité d'apply
- Seules les valeurs par défauts sont mises à jours, les valeurs spécifiques instance sont indiquées en gras dans l'interface

Pour instancier dynamiquement un prefab, il faudra créé une propriété `public GameObject myPrefab;` dans le script qui doit l'utiliser. Ensuite, il est possible de faire à tout moment `var instance = Instantiate(myPrefab)` pour ajouter l'élément sur la scène dynamiquement. La variable "instance" pourra être modifiée directement pour changer certaines propriétés du gameObject (par exemple sa position).

### Conseils de collaboration

https://teachings.samuel-bouchet.fr/jeu-video/4_fichespratiques#/4/2

- Une scène par collaborateur (Très difficile à fusionner à la main)
- Chacun développe ses prefabs, scripts et sa scène de façon indépendante
- 1 personne fait l'intégration dans la scène générale du jeu

### Interactions entre scripts

```CS
private ScriptName scripteRef;

void Start () {
    // Récupère une référence au script ScriptName attaché au même GameObject
    scripteRef = GetComponent<scriptname> ();
    scripteRef.someMethodOfScriptName();
}
```

### Hiérarchie - références et parents

```CS
// un gameObject de la scène, on réfère ici son Transform
public Transform otherGameObjectTransform;
// on affectera dans scripteRef le ScriptName de ce gameObject
private ScriptName scripteRef;

// un 2eme game object de la scène, on réfère ici le GameObject
public GameObject otherGameObject2;
// on affectera dans scripteRef2 le ScriptName de ce 2eme gameObject
private ScriptName scripteRef2;

void Start () {
    // scripteRef attaché à otherGameObjectTransform
    scripteRef = otherGameObjectTransform.gameObject.GetComponent<scriptname> ();
    Transform parent = otherGameObjectTransform.parent;

    // scripteRef2 attaché à otherGameObject2
    scripteRef2 = otherGameObject2.GetComponent<scriptname> ();
    Transform parent2 = otherGameObject2.transform.parent;
}
```

### Hiérarchie - Parcourir les enfants

```CS
void Start () {
    // on cherche à trouver les ScriptName dans les enfants, mais le parcours d'enfant
    // peut servir à plein de choses.
    // méthode 1, si on souhaite utiliser LinQ sur la collection d'enfants
    IEnumerable<Transform> children = (IEnumerable<Transform>) this.transform;
    foreach (Transform child in children) {
        child.gameObject.GetComponent<ScriptName>(); // peut être null !
    }
    // méthode 2, plus courte si on veut juste itérer
    foreach (Transform child in this.transform) {
        child.gameObject.GetComponent<ScriptName>(); // peut être null !
    }
    // méthode 3, si on cherche spécifiquement un script
    foreach (ScriptName script in this.GetComponentsInChildren<ScriptName>()) {
        // script n'est pas null
    }
}
```

### Audio

- AudioSource = Haut parleur qui peut lire un son à la fois.
- C'est un composant à attacher à un game objet, il sera spacialisé à l'endroit de l'objet.
- AudioClip = Clip audio qui peut être lu par un Audio source.
- On peut changer le AudioClip d'un AudioSource par script.
- Le son est capturé par l'AudioListener, positionné par défaut sur la caméra.

### [ExecuteInEditMode]

L'annotation [ExecuteInEditMode] permet d'exécuter un script en mode édition. Ex:

- [DepthSortByY](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/BallGameNetworked/Scripts/DepthSortByY.cs)
- [SnapToGrid](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/samuelb/Scripts/SnapToGrid.cs)
- [LiffebarController](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/samuelb/Scripts/LifebarController.cs)
- [Track](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/samuelb/Scripts/Track.cs)

### Barre de vie

- GameObject parent pour positionner le tout
  - GameObject BackgroundScaler pour étendre la barre vide
    - SpriteRenderer BackgroundSprite avec décalage de 0.5 vers la droite pour décaler le point d'origine du scaling
  - GameObject ForegroundScaler pour étendre la barre pleine
    - SpriteRenderer ForegroundSprite avec décalage de 0.5 vers la droite pour décaler le point d'origine du scaling
- Ajouter un script pour piloter la taille des éléments visuels (Ex: [LifebarController](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/samuelb/Scripts/LifebarController.cs))

<div style="text-align:center">
<video controls width="720" height="405">
    <source src="LifebarController_h1n8jl.mp4" type="video/mp4" />
</video>
</div>

### Exécuter une action étendue sur plusieurs frames / durées

Appeler une fois :

```CS
StartCoroutine (DoRotateByStep ());
```

Effet avec boucle :

```CS
IEnumerator DoRotateByStep () {
    Quaternion originalRotation = this.transform.rotation;
    int durationInFrames = 20;
    for (int i = 0; i < durationInFrames; i++) {
        this.transform.rotation = Quaternion.Euler (0, 0, (i * 360f) / durationInFrames);
        yield return null; // attends la prochaine frame pour continuer
    }
    this.transform.rotation = originalRotation;
}
```

Exemple plus complexe: [Shake](https://github.com/Lythom/JeuVideo/blob/26841377c41347fc4d5d0049fa09b2d775a866ff/Assets/BallGameNetworked/Scripts/Shake.cs)

---

Envie de réagir ? Mentionnez [@Lythom](https://twitter.com/lythom) dans votre tweet ou envoyer un mail à [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).
