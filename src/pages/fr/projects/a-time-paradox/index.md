---
title: A Time Paradox
date: "2018-08-27"
description: Vous êtes coincé dans une boucle temporelle. Infiltrez-vous contre vous-même pour éviter le paradoxe !
---

Vous êtes coincé dans une boucle temporelle. Infiltrez-vous contre vous-même pour éviter le paradoxe !

<div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="320" height="320">
        <source src="atimeparadox_loop.mp4" type="video/mp4" />
    </video>
</div>

> Je dois me cacher des moi du passé. Si je suis vu…  
> <cite>Un voyageur temporel</cite>

<div class="startButton">
    <a href="https://a-time-paradox.com/">Voir le site</a>
</div>
