---
title: Upcoming update for A Time Paradox
date: "2019-11-11"
description: Discover what's next in A Time Paradox
---

A Time Paradox development is still going on! If you haven't heard of it, it's a puzzle game I'm working on based on infiltration against one-self.
Because the project is self financed, progress is depending on other jobs and life stuff. So how is A Time Paradox going may you ask? Reaching some key points!

## In-game

Android freanch early access version is still available on the Play Store, but it's ending soon. Challenge mode is now technically implemented, and will provided even harder challenges! The Story mode was made for smooth progression, and challenge mode to explore the limits of your mind.

<figure>
  <img src="defi_example.jpg" alt="">
  <figcaption style="text-align:center">Some upcoming challenge</figcaption>
</figure>

You'll also be given the opportunity to create your own level. I put it in for amusement, but could become a ccommunity creation tool in the future.

<figure>
        <div style="text-align:center">
<video autoplay="autoplay" loop="loop" width="480" height="auto">
        <source src="editor_showcase_480.mp4" type="video/mp4" />
    </video>
</div>
  <figcaption style="text-align:center">Level editor showcase</figcaption>
</figure>

Finally, I had to tackle some big stuff like the hidden part of the iceberg, you'll be asked if you want to synchronize your data using an "A Game Studio" account. Passwordless, the account will allow you to synchronize you data, scores and collected keys across devices. Also, you personnal data (email, game data) won't be shared with 3rd party companies and will be kept secret on A Game Studio servers in Europe.

<figure>
        <div style="text-align:center">
<video autoplay="autoplay" loop="loop" width="360" height="auto">
        <source src="unlock_levels.mp4" type="video/mp4" />
    </video>
    </div>
  <figcaption style="text-align:center">Unlock all the challenges!</figcaption>
</figure>

Challenges will unlock by collecting keys. There are several ways to get them like in-game levels and weekly deliveries. Also, you'll never have to purchase "keys" directly. The promise is that once you bought the game, you have it all, and the content must be earned the hard way!

## On the web

Playing alone is cool, sharing the experience is greater! A Time Paradox will also be living on [Discord](https://discord.gg/UVZ27gA)), on [Twitter](https://twitter.com/AGameStudio)), and on [Facebook](https://www.facebook.com/atimeparadox/). You only want news ? Check the [blog RSS](https://a-game-studio.com/rss.xml) or [the newsletter](https://a-time-paradox.com/#mc_embed_signup_scroll).

## On your PC

This update is also a first step toward PC release! Android update will come in the upcoming weeks, Pc version will have to wait some extra weeks after that. It will contains some exclusive game mode that wouldn't fit mobile…

If you prefer playing on PC, feel free to whishlist the game now on [A Time Paradox Steam Page](https://store.steampowered.com/app/1167730/A_Time_Paradox/).

<figure>
  <img src="in_library.png" alt="A Time Paradox in Steam library">
  <figcaption style="text-align:center">Soon in your library</figcaption>
</figure>

I wish A Time Paradox raise your interest as much as it passionate me! See you soon in time adventures ;)

---

Want to respond to this article? Let me know!  
PM or mention [@Lythom](https://twitter.com/lythom) or drop a message at [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).
