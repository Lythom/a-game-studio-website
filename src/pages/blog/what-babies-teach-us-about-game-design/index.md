---
title: What babies teach us about game design
date: "2018-09-14"
description: Find out how studies of our brain development and emotions help us as game designers.
---

> "Between twenty and fifty times a day, the little child experiences a state of great enthusiasm. (...) Each small storm of enthusiasm implements a kind of cerebral autodoping. In this way, the substances necessary for all processes of growth and redevelopment of neural networks are produced. That is why we are learning so quickly in what we are doing with enthusiasm. Because it's as simple as that: the brain develops precisely where it is enthusiastically used." <cite>Pr. Gerald Hüther</cite>

<figure>
  <img src="lego-3388163_640.png" alt="Drawing of a construction game for children">
  <figcaption>Illustration by <a href="https://pixabay.com/fr/users/Painter06-3732158/">Francis Ray</a></figcaption>
</figure>

## On brain development

In addition to telling us about the mechanics of human growth, which are invaluable in order to guide our childs toward a better development, Professor Gerald Hüther's "Neurobiology and Education" conference sheds light on a specific brain mechanic linked to learning.

Studies show[^1]<sup>,</sup>[^2] that spikes of enthusiasm causes a spill of neuroplastic transmitters. In the same way as fertilizer, their role is to promote brain development. Three important things should be noted:

- only the actually _active areas_ of the brain are affected by this development,
- it only happens when you feel a powerful emotion,
- this biomechanics phenomena works regardless of age.

An anecdote from the author, relevant in our context:

> « Our young people have had, for the past 10 years, a region of the brain that receives so much fertilizer that it has already doubled in size: this is the region that is responsible for regulating thumb movements. <cite>Pr. Gerald Hüther</cite>

The take away is that to convey a message, encourage certain behaviours, promote the memorization of important elements, the player must be emotionally affected.

## On social exclusion

We also learn in this conference that when we experience isolation or social exclusion, it activates the same neural circuits as when we are inflicted bodily suffering.

With this in mind, the inclusive part of a game becomes essential: it is not moraly possible to produce a game that creates suffering.

From my experience, a concrete example is the [Dixit](https://fr.asmodee.com/fr/games/dixit/) board game. In this game the goal is to make some players guess a card, but not all. As a result, players who already have strong social relationships tend to strengthen their relations even more, but players who are not already part of the social group majority are excluded as a result of the game mechanics.

<figure>
  <img src="mysterium.jpg" alt="Card from Mysterium">
  <figcaption style="text-align:center">Example of "vision" cards from Mysterium</figcaption>
</figure>

Aware of this, the flaw is corrected in [Mysterium](https://fr.asmodee.com/fr/games/mysterium/). Identical in its mechanics (making people guess cards), the collaborative approach naturally encourages players to know each other better in order to succeed. The designers have managed to keep the fun game mechanics (Dixit remains a very good game!) while reversing the relationships between the players: the new game objectives promote empathy even more (players try to imagine how the other players will perceive a visual) and the discriminating mechanics disappears (even if only a part of the group guesses, the whole group benefits from the discovery).

## For us and for all

In relationships with our children and players alike, we benefit from a better understanding of how the brain works in order to provide more fulfilling experiences. As designers (but not only), neuroscience probably still has a lot to teach us. Let's adapt to this new knowledge!

---

Do you want to react? Mention [@Lythom](https://twitter.com/lythom) in your tweet or send an email to [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).

[^1]: [Natures and value - Pr. Dr. Gerald Hüther](https://www.ecologiedelenfance.com/app/download/14155822524/nature_and_values.pdf?t=1492608224)
[^2]: [Neurobiologie et éducation : conférence du Prof. Dr. Gerald Hüther (VOSTFR)](https://youtu.be/IGQ9i-xdruc)
