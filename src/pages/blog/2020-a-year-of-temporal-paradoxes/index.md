---
title: 2020, a year of temporal paradoxes?
date: "2020-01-03"
description: A Time Paradox update content, Happy new year!
---

Happy new year! Is training your brain some part of this year good resolutions? Then give a try to this new A Time Paradox update on Android!

[A Time Paradox on Play Store](https://play.google.com/store/apps/details?id=studio.agame.atimeparadox)

## English version

A Time Paradox is now available in most country world wide on the Android Play Store. You can enjoy the game as long as you can speak english or french!

<figure>
        <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="720" height="auto" controls>
        <source src="menurotate_720.mp4" type="video/mp4" />
    </video>
</div>
  <figcaption style="text-align:center">Rotating menu of A Time Paradox</figcaption>
</figure>

## Play with a friend!

A new cooperation campaign is available as preview. 10 levels are already there to play with a friend (use landscape mode), more will come!

## Story mode rebalancing

Previously stuck? Give it another try! There is even challenge keys to collect now.

## Challenges

Those keys will allow you to unlock new challenges levels. If you already completed the game, here is more content to play with!

A bunch of keys will be distributed weekly when you start the game! (required an internet connection)

## Level editor

The level editor will allow you to create your own levels and play with the game engine itself. Come post your levels on the [Discord](https://discord.gg/UVZ27gA)!

<figure>
        <div style="text-align:center">
    <video autoplay="autoplay" loop="loop" width="480" height="auto" controls>
        <source src="editor_showcase_480.mp4" type="video/mp4" />
    </video>
</div>
  <figcaption style="text-align:center">Level editor showcase</figcaption>
</figure>

## Account creation and online backup

After you bought the game, you can create a passwordless "A Game Studio" account from the game. This will sync your progress and acquired challenge keys when connected to the internet. The game will remain 100% playable offline.

## Sound toggle

I bet a lot of you like to play with your favorite musics!

# Have fun!

I can't wait to get feedbacks on all this new stuff, Have Fun with the game!

Samuel

[https://a-time-paradox.com/](https://a-time-paradox.com/)

---

Want to respond to this article? Let me know!  
PM or mention [@Lythom](https://twitter.com/lythom) or drop a message at [samuel@a-game-studio.com](mailto:samuel@a-game-studio.com).
