---
title: Press Start Button
date: "2018-08-26"
---

A Game Studio is opening the blog ! You will find news, technical articles about game programming, shaders, game design and any topic an indie game dev could find interesting.

I can wait ! I hope you'll like it.

No more waiting, here is the first technical article. Let the fun begin !

<div class="startButton">
    <a href="../hacking-through-shaders">Start</a>
</div>
